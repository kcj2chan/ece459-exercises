use std::sync::mpsc;
use std::thread;
use std::time::Duration;

static N: i32 = 10;  // number of threads to spawn

// You should modify main() to spawn threads and communicate using channels
fn main() {
    let (tx, rx) = mpsc::channel();
    let mut children = vec![];
    for x in 1..=N {
        let clone = mpsc::Sender::clone(&tx);
        children.push(thread::spawn(move || {
            let message = format!("Message from thread {}.", x);
            clone.send(message).unwrap();
            thread::sleep(Duration::from_secs(1));
        }));
    }
    for received in rx {
        println!("Got: {}", received);
    }
}
