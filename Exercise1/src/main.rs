// You should implement the following function:

fn sum_of_multiples(mut number: i32, multiple1: i32, multiple2: i32) -> i32 {
    let mut total = 0;
    for current in 0..number {
        if current % multiple1 == 0 || current % multiple2 == 0 {
            total += current;
        }
    }
    total
}

fn main() {
    println!("{}", sum_of_multiples(1000, 5, 3));
}
