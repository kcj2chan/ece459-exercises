// You should implement the following function
// Using dynamic programming
// fn fibonacci_number(n: u32) -> u32 {
//     if n == 0 || n == 1 { return 1 }
//
//     let n : usize = n as usize;
//     let mut dp = Vec::new();
//     dp.push(0);
//     dp.push(1);
//     for x in 2..=n {
//         dp.push(dp[x - 1] + dp[x - 2]);
//     }
//     *dp.last().unwrap()
// }

// Recursively
fn fibonacci_number(n: u32) -> u32 {
    match n {
        0 | 1 => n,
        _ => {
            fibonacci_number(n - 1) + fibonacci_number(n - 2)
        }
    }
}

fn main() {
    println!("{}", fibonacci_number(10));
}
